<?php
class User_model extends CI_Model{
	function login_auth($data){
		$this->db->where('USERNAME', $data['USERNAME']);
		$this->db->where('PASSWORD', $data['PASSWORD']);
		$query = $this->db->get('USER_ACCOUNT');
		if($query -> num_rows() == 1)
	   {
	     return true;
	   }
	   else
	   {
	     return false;
	   }
	}
	function account_model($data){
		$this->db->insert('USER_ACCOUNT', $data);
		return;
	}
	function get_accounts(){
		$query=$this->db->get('USER_ACCOUNT');
		return $query->result();
	}
	function addnew_post($data){
		$this->db->insert('NEWS', $data);
		return;
	}
	/*function get_post(){
		$query=$this->db->get('NEWS');
		return $query->result();
	}*/
	function delete_account(){
		$this->db->where('ID', $this->uri->segment(3));
		$this->db->delete('USER_ACCOUNT');
		return;
	}
	function edit_account($id){
		$this->db->where('ID', $id);
		$query=$this->db->get('USER_ACCOUNT');
		if($query->num_rows()){
			return $query->row_array();
		}else{
			$msg['error']="User Not Found!";
			return $msg;
		}
		
	}
	function save_user($user_details, $id){
		$this->db->where('ID', $id);
		$query=$this->db->update('USER_ACCOUNT', $user_details);
		return;
	}

	//for displaying post///
	function totalcountPost(){
		$count = $this->db->count_all('NEWS');
		return $count;
	}
	function display_post($start, $limit){
		$query = $this->db->get('NEWS', $limit, $start);
		return $query->result();
	}
	function getlatestpost(){
		$limit = 1;
		$this->db->select('NEWS_ID');
		$this->db->order_by('NEWS_ID', 'DESC');
		$query = $this->db->get('NEWS', $limit);
		return $query->result();
	}
	function getContent($id){
		$this->db->where('NEWS_ID', $id);
		$query = $this->db->get('NEWS');
		return $query->result();
	}
	function getpostTitle(){
		$query = $this->db->get('NEWS');
		return $query->result();
	}
	function get_post($limit, $offset){
		$this->db->order_by('NEWS_ID', 'DESC');
		$query = $this->db->get('NEWS', $limit, $offset);
		return $query->result();
	}
}


?>