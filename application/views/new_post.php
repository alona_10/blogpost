<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Add Post</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles.css');?>">
</head>
<body>

<div id="container">
	<div id="body">
		<?php
		error_reporting(0);
			if($links){
				echo $links;
			}
		?>
		<div id="contents" style="width:50%;">
		<?php echo form_open_multipart('welcome/new_post');?>
		<br>
			<table>
				<tr><td colspan="2" id="userhead"><b>Add Post Form</b></td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td id="userlabel">Date: </td>
					<td>
						<input type="text" name="date" value="<?php echo date("F j, Y"); ?>" style="border:none;">
					</td>
				</tr>
				<tr><td id="userlabel">Title: </td> <td><input type="text" name="title" value="<?php if(isset($_POST['title'])){ echo $_POST['title'];}?>"></td></tr>
				<tr><td id="userlabel" valign="top">Content: </td> <td>
					<textarea name="content" id="post_content">
						<?php if(isset($_POST['content'])){ echo $_POST['content'];}?>
					</textarea>
				</td></tr>
				<tr>
					<td id="userlabel">Post Image:</td>
					<td><input type="file" name="post_image" size="20" /></td>
				</tr>
				<tr>
					<td id="userlabel">Post PDF:</td>
					<td><input type="file" name="post_pdf" size="20" /></td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="submit" value="Save Post">
						<input type="reset" name="reset" value="Clear">
					</td>
				</tr>
				<tr>
					<td colspan="2" id="errors">
						<?php echo validation_errors(); ?>
					</td>
				</tr>
			</table>
			</br></br></br></br></br>
		</div>
		<?php echo form_close();?>
	</div>
</div>

</body>
</html>