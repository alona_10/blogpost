<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>View Posts</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles.css');?>">
</head>
<body>

<div id="container">
	<div id="body">
		<?php
		error_reporting(0);
			if($links){
				echo $links;
			}
		?>
	
		<div style="width:80%; margin-top:10px;">
			<div id="viewpost-left" style="float:left; width:27%; height:100%; border:0px solid black;">
				<center><h1>NEWS TITLES</h1></center>
				<?php 
				if(isset($posts)){
					$pageno = $offset;
					foreach ($posts as $titles) {
						$news_id = $titles->NEWS_ID;
						$headline = $titles->HEADLINE;
						$date = $titles->DATE;
						echo "<a id='linkposts' href=".base_url('index.php/welcome/post_paginates/?pageno='.$offset.'&post_id='.$news_id).">";
						echo $headline . "</a></br>";
						echo $date;
						echo "<br>";
					}
				}
				?>
				<?php 
					echo "<div id='pages'>".$pages."</div>";
				?>
			</div>

			<div id="viewpost-right" style="float:right; width:70%; border:0px solid black;">
				<?php
				if(isset($content)) {
					foreach ($content as $value) {
						echo "<h1 align='center'>". $value->HEADLINE."</h1>";
						echo "<p style='text-align:justify'>".$value->CONTENT."</p>";
						echo "</br></br>";
						echo "<p align='right'><i>Posted on - ". $value->DATE ."</i></p>";
					}
				}
				?>
			</div>
		</div>
	</div>
</div>

</body>
</html>