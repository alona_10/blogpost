<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles.css');?>">
</head>
<body>
	<div id="container" style="border-radius:5px; box-shadow: #ccc 0px 0px 5px; width: 35%; padding: 15px;">
		<?php echo form_open('welcome/login');?>
		<table align="center">
			<tr>
					<td colspan="2" id="errors">
						<?php if(isset($errormessage)){ echo $errormessage;} ?>
					</td>
				</tr>
			<tr><td colspan="2" align="center"><b>LOGIN FORM</b></td></tr>
			<tr>
				<td>Username:</td>
				<td><input type="text" name="username" value="<?php if(isset($USERNAME)){ echo $USERNAME;}?>"></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" value="<?php if(isset($PASSWORD)){ echo $PASSWORD;}?>"></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="submit" value="Login">
					<input type="reset" name="cancel" value="Clear">
				</td>
			</tr>
		</table>

		<?php echo form_close(); ?>
	</div>

</body>
</html>