<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Edit User</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles.css');?>">
</head>
<body>

<div id="container">
	<div id="body">
		<?php
		error_reporting(0);
			if($links){
				echo $links;
			}
		?>
		<div id="contents">
		<?php 
		if(isset($error)){
			echo $error;
			redirect(base_url('index.php/welcome/view_accounts'));
		}
		$id=$this->uri->segment(3);
		echo form_open('welcome/save_account/'.$id);?>
		<br>
			<table>
				<tr><td colspan="2" id="userhead"><b>Edit User Form</b></td></tr>
				<tr><td id="userlabel">First Name: </td> <td><input type="text" name="fname" 
				value="<?php if(isset($FNAME)){ echo $FNAME; }?>"></td></tr>

				<tr><td id="userlabel">Last Name: </td> <td><input type="text" name="lname" 
				value="<?php if(isset($LNAME)){ echo $LNAME; }?>"></td></tr>

				<tr><td id="userlabel">Email Address: </td> <td><input type="text" name="email" 
				value="<?php if(isset($EMAIL)){ echo $EMAIL; }?>"></td></tr>

				<tr><td id="userlabel">Username: </td> <td><input type="text" name="username" 
				value="<?php if(isset($USERNAME)){ echo $USERNAME; }?>"></td></tr>

				<tr><td id="userlabel">Password: </td> <td><input type="password" name="password" 
				value="<?php if(isset($PASSWORD)){ echo $PASSWORD; }?>"></td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="submit" value="Save Changes">
						<a href="<?php echo base_url('welcome/view_accounts');?>"><button>Back</button></a>
					</td>
				</tr>
				<tr>
					<td colspan="2" id="errors">
						<?php echo validation_errors(); ?>
					</td>
				</tr>
			</table>
			</br></br></br></br></br>
		</div>
		<?php echo form_close();?>
	</div>
</div>

</body>
</html>