<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles.css');?>">
</head>
<body>

<div id="container">
	<div id="body">
		<div id="links">
		<p>
			<?php
			if($username){
				echo "Welcome <b>".$username."</b>! ";
				?>
				<a href="<?php echo base_url('index.php/welcome/logout'); ?>">Logout</a>
			<?php
			}
			?>
		</p>
		
		<?php
		error_reporting(0);
			if($links){
				echo $links;
			}
		?>
		</div>
		<div id="contents">
			<p>
				This is the official website for Alona Ponce's blogpost!
			</p>
		</div>
	</div>
</div>

</body>
</html>