<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>View Accounts</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles.css');?>">
</head>
<body>

<div id="container">
	<div id="body">
		<?php
		error_reporting(0);
			if($links){
				echo $links;
			}
		?>

		<div id="contents" style="width:50%">
			<?php
				if(isset($records)){
			?>
				<table style="width:100%;">
					<tr>
						<td><b>Name</b></td>
						<td><b>Email Address</b></td>
						<td><b>Username</b></td>
						<td><b>Actions</b></td>
					</tr>
						<?php
						foreach ($records as $row) {
							echo "<tr>";
							echo "<td>".$row->FNAME." ".$row->LNAME."</td>";
							echo "<td>".$row->EMAIL."</td>";
							echo "<td>".$row->USERNAME."</td>";
							echo "<td>";?>
								<a href="<?php echo 'edit_acc/'. $row->ID;?>"><?php
								echo "<img src='".base_url('public/images/icons/pencil_edit.png')."' id='action_icon' title='Edit Account'/>";
								echo "</a>&nbsp;&nbsp;"; ?>

								<?php
								/*echo "<img src='".base_url('public/images/icons/trash.png')."' id='action_icon' title='Delete Account'/>";
								echo "</a>";*/

								echo anchor("welcome/del_acc/".$row->ID,"<img id='action_icon' title='Delete Account' src='".base_url('public/images/icons/trash.png')."'",array('onclick' => "return confirm('Do you want delete this record?')"));
							echo "</td>";
							echo "</tr>";
						}
						?>
				</table>
			<?php
				}
				if(isset($message)){
					echo $message;
				}
			?>
		</div>
	</div>
</div>

</body>
</html>