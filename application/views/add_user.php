<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Add User</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/styles.css');?>">
</head>
<body>

<div id="container">

	<div id="body">
		<div id="links">
		<?php
		error_reporting(0);
			if($links){
				echo $links;
			}
		?>
		<div id="contents">
		<?php echo form_open('welcome/add_account');?>
		<br>
			<table>
				<tr><td colspan="2" id="userhead"><b>Add User Form</b></td></tr>
				<tr><td id="userlabel">First Name: </td> <td><input type="text" name="fname" value="<?php if(isset($_POST['fname'])){ echo $_POST['fname'];}?>"></td></tr>
				<tr><td id="userlabel">Last Name: </td> <td><input type="text" name="lname" value="<?php if(isset($_POST['lname'])){ echo $_POST['lname'];}?>"></td></tr>
				<tr><td id="userlabel">Email Address: </td> <td><input type="text" name="email" value="<?php if(isset($_POST['email'])){ echo $_POST['email'];}?>"></td></tr>
				<tr><td id="userlabel">Username: </td> <td><input type="text" name="username" value="<?php if(isset($_POST['username'])){ echo $_POST['username'];}?>"></td></tr>
				<tr><td id="userlabel">Password: </td> <td><input type="password" name="password" value="<?php if(isset($_POST['password'])){ echo $_POST['password'];}?>"></td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="submit" value="Save User">
						<input type="reset" name="reset" value="Clear">
					</td>
				</tr>
				<tr>
					<td colspan="2" id="errors">
						<?php echo validation_errors(); ?>
					</td>
				</tr>
			</table>
			</br></br></br></br></br>
		</div>
		<?php echo form_close();?>
	</div>
</div>

</body>
</html>