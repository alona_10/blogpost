<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
    {
        parent::__construct();
       	/*if($this->session->userdata('logged_in')){
       		echo "session was set";
        	//redirect('welcome/login_view');
        }
        else{
        	echo "session not set";
        	redirect('welcome/login_view');
        }*/
    }

	public function index()
	{
		if($this->session->userdata('logged_in'))
	   	{
	     	$session_data = $this->session->userdata('logged_in');
	     	$data['links'] = $this->load->view('links');
	     	$data['username'] = $session_data['USERNAME'];
	     	$this->load->view('home', $data);
	   	}
	   	else
	   	{
	     	redirect('welcome/login_view');
	   	}
	}
	function login_view(){
		$this->load->view('login');
	}
	function login(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()==FALSE){
			$data['errormessage']="Check your username and password.";
			$this->load->view('login', $data);
		}else{
			$data=array(
				'USERNAME' => $this->input->post('username'),
				'PASSWORD' => $this->input->post('password')
			);

			$this->load->model('User_model');

			$result = $this->User_model->login_auth($data);

			if($result==true){
				$this->session->set_userdata('logged_in', $data);
				$data['links'] = $this->load->view('links');
				$data['username'] = $this->input->post('username');
				$this->load->view('home', $data);
			}else{
				$data['errormessage']="Invalid Username or Password";
				$this->load->view('login', $data);
				//redirect('welcome/login_view');
			}
		}
	}
	function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		//$this->load->view('login_view');
		redirect('welcome/login_view');
	}
	function add_user(){
		$include['links'] = $this->load->view('links');
		$this->load->view('add_user', $include);
	}
	function view_accounts(){
		$data=array();
		
		$this->load->model('User_model');
		if($query=$this->User_model->get_accounts()){
			$data['records']=$query;
			$data['links'] = $this->load->view('links');
		}

		$this->load->view('user_accounts', $data);
	}
	function add_account(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()==FALSE){
			$include['links'] = $this->load->view('links');
			$this->load->view('add_user', $include);
		}else{
			$data=array(
				'FNAME' => $this->input->post('fname'),
				'LNAME' => $this->input->post('lname'),
				'EMAIL' => $this->input->post('email'),
				'USERNAME' => $this->input->post('username'),
				'PASSWORD' => $this->input->post('password')
			);
			$this->load->model('User_model');
			$this->User_model->account_model($data);
			$this->view_accounts();
			//$this->load->view('user_accounts');
		}
	}
	function add_post(){
		$include['links'] = $this->load->view('links');
		$this->load->view('new_post', $include);
	}
	function view_post(){
		$data=array();
		$this->load->model('User_model');
		if($query=$this->User_model->get_post()){
			$data['records']=$query;
			$data['links'] = $this->load->view('links');
		}
		$this->load->view('view_post', $data);
	}
	function new_post(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('new_post');
		}else{
			$data=array(
				'HEADLINE' => $this->input->post('title'),
				'CONTENT' => $this->input->post('content'),
				'DATE' => $this->input->post('date')
			);
			$this->load->model('User_model');
			$this->User_model->addnew_post($data);
			$this->view_post();
			//$this->load->view('view_post');

		}
	}
	function del_acc($id){
		if(! $id){
			redirect('welcome/view_accounts');
			return;
		}
		$this->load->model('User_model');
		$this->User_model->delete_account();
		redirect('welcome/view_accounts');
	}
	function edit_acc($id=''){
		if(! $id){
			redirect('welcome/view_accounts');
			return;
		}

		$this->load->model('User_model');
		$result=$this->User_model->edit_account($id);
		
		if(isset($msg)){
			$this->load->view('edit_user', $msg);
		}else{
			$this->load->view('edit_user', $result);
		}
	}
	function save_account($id){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()==FALSE){
			$this->edit_acc($id);
		}else{
			$data=array(
				'FNAME' => $this->input->post('fname'),
				'LNAME' => $this->input->post('lname'),
				'EMAIL' => $this->input->post('email'),
				'USERNAME' => $this->input->post('username'),
				'PASSWORD' => $this->input->post('password')
			);
			$this->load->model('User_model');
			$this->load->library('session');

			$this->User_model->save_user($data, $id);

			$this->session->set_flashdata('message', 'Changes has been saved!');
			redirect('welcome/view_accounts');
		}
	}
	///for displaying the posts
	function post_paginates($offset = 0, $id=''){
		$this->load->model('User_model');
		$this->load->library('pagination');

		$limit = 3;
		$config['base_url'] = 'http://localhost/blogpost/index.php/welcome/post_paginates/';
		$config['total_rows'] = $this->User_model->totalcountPost();
		$config['per_page'] = $limit;
		$config['num_links'] = 2;
		$config['uri_segment'] = 3;
		$config['page_query_string']=true; 
		$config['query_string_segment'] = 'pageno';
		$offset = $this->input->get('pageno'); 
		$id = $this->input->get('post_id');

		if(!isset($id)){
			$id = $this->User_model->getlatestpost();
			foreach ($id as $value) {
				$id = $value->NEWS_ID;
			}
			$result= $this->User_model->getContent($id);
		}else{
			$result= $this->User_model->getContent($id);
		}

		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['cur_tag_open'] = "<a class='active'>";
		$config['cur_tag_close'] = "</a>";
		
		$this->pagination->initialize($config);

		$posts = $this->User_model->get_post($limit, $offset);
		$data = array(
			'posts' => $posts,
			'pages' => $this->pagination->create_links(),
			'offset' => $offset,
			'content' => $result,
			'links' => $this->load->view('links')
		);

		$this->load->helper('date');
		//echo date("F j, Y, g:i a"); to display current date and time

		$this->load->view('view_post', $data);
	}
	function fileupload(){
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '1000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('message' => $this->upload->display_errors());
			$error['links'] = $this->load->view('links');
			$this->load->view('upload_file', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data['message2'] = "File Uploaded Successfully";
			$data['links'] = $this->load->view('links');
			$this->load->view('upload_file', $data);
		}
	}
	function upload(){
		$data['links'] = $this->load->view('links');
		$this->load->view('upload_file', $data);
	}
}
